/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.calshape;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Aritsu
 */
public class RecFrame extends JFrame{
    JLabel lblWidth;
    JLabel lblHeight;
    JTextField txtWidth;
    JTextField txtHeight;
    JButton btnCal;
    JLabel lblResult;
    public RecFrame(){
        super("Rectangle");
        this.setSize(500,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblWidth = new JLabel("width:",JLabel.TRAILING);
        lblWidth.setSize(45, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.white);
        lblWidth.setOpaque(true);
        this.add(lblWidth);
        
        lblHeight = new JLabel("height:",JLabel.TRAILING);
        lblHeight.setSize(45, 20);
        lblHeight.setLocation(5, 35);
        lblHeight.setBackground(Color.white);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
        
        txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(55, 5);
        this.add(txtWidth);
        
        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(55, 35);
        this.add(txtHeight);
        
        btnCal = new JButton("Calculate");
        btnCal.setSize(100, 20);
        btnCal.setLocation(120, 5);
        this.add(btnCal);
        
        lblResult = new JLabel("Rectangle w= ?? h= ?? area= ?? perimeter= ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400,50);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        lblResult.setLocation(0, 80);
        this.add(lblResult);
        
        btnCal.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strWidth = txtWidth.getText();
                String strHeight = txtHeight.getText();
                double Width = Double.parseDouble(strWidth);
                double Height = Double.parseDouble(strHeight);
                Rectangle rectangle = new Rectangle(Width,Height);
                
                lblResult.setText("Rectangle w = "+ String.format("%.2f", rectangle.getWidth()) 
                        + " h = " + String.format("%.2f", rectangle.getHeight())
                        + " area = " + String.format("%.2f", rectangle.calArea())
                        + " perimeter = "+ String.format("%.2f", rectangle.calPerimeter()));
                } catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(RecFrame.this, "Error: Please input Number"
                            , "Error!",JOptionPane.ERROR_MESSAGE );
                   txtWidth.setText("");
                   txtWidth.requestFocus();
                   txtHeight.setText("");
                   txtHeight.requestFocus();
                }
       
            }
        });
    }
     public static void main(String[] args) {
        RecFrame frame = new RecFrame();
        frame.setVisible(true);
    }
}
