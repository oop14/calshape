/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.calshape;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Aritsu
 */
public class SquareFrame extends JFrame{
    JLabel lblSide;
    JTextField txtSide;
    JButton btnCal;
    JLabel lblResult;
    public SquareFrame(){
        super("Square");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblSide = new JLabel("side:",JLabel.TRAILING);
        lblSide.setSize(45, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.white);
        lblSide.setOpaque(true);
        this.add(lblSide);
        
        txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(55, 5);
        this.add(txtSide);
        
        btnCal = new JButton("Calculate");
        btnCal.setSize(100, 20);
        btnCal.setLocation(120, 5);
        this.add(btnCal);
        
        lblResult = new JLabel("Square side= ?? area = ?? perimeter = ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300,50);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        lblResult.setLocation(0, 50);
        this.add(lblResult);
        
        btnCal.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strSide = txtSide.getText();
                double side = Double.parseDouble(strSide);
                Square square = new Square(side);
                lblResult.setText("Square s = "+ String.format("%.2f", square.getSide())  
                        + " area = " + String.format("%.2f", square.calArea())
                        + " perimeter = "+ String.format("%.2f", square.calPerimeter()));
                } catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error: Please input Number"
                            , "Error!",JOptionPane.ERROR_MESSAGE );
                   txtSide.setText("");
                   txtSide.requestFocus();
                }
            }
        });
    }
    
    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame(); 
        frame.setVisible(true);
    }
}
