/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.calshape;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Aritsu
 */
public class TriFrame extends JFrame{
    JLabel lblBase;
    JLabel lblHeight;
    JTextField txtBase;
    JTextField txtHeight;
    JButton btnCal;
    JLabel lblResult;
    public TriFrame(){
        super("Triangle");
        this.setSize(500,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblBase = new JLabel("base:",JLabel.TRAILING);
        lblBase.setSize(45, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.white);
        lblBase.setOpaque(true);
        this.add(lblBase);
        
        lblHeight = new JLabel("height:",JLabel.TRAILING);
        lblHeight.setSize(45, 20);
        lblHeight.setLocation(5, 35);
        lblHeight.setBackground(Color.white);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
        
        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(55, 5);
        this.add(txtBase);
        
        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(55, 35);
        this.add(txtHeight);
        
        btnCal = new JButton("Calculate");
        btnCal.setSize(100, 20);
        btnCal.setLocation(120, 5);
        this.add(btnCal);
        
        lblResult = new JLabel("Triangle b= ?? h= ?? area= ?? perimeter= ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400,50);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        lblResult.setLocation(0, 80);
        this.add(lblResult);
        
        btnCal.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strBase = txtBase.getText();
                String strHeight = txtHeight.getText();
                double base = Double.parseDouble(strBase);
                double height = Double.parseDouble(strHeight);
                Triangle triangle = new Triangle(base,height);
                
                lblResult.setText("Triangle b = "+ String.format("%.2f", triangle.getBase()) 
                        + " h = " + String.format("%.2f", triangle.getHeight())
                        + " area = " + String.format("%.2f", triangle.calArea())
                        + " perimeter = "+ String.format("%.2f", triangle.calPerimeter()));
                } catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(TriFrame.this, "Error: Please input Number"
                            , "Error!",JOptionPane.ERROR_MESSAGE );
                   txtBase.setText("");
                   txtBase.requestFocus();
                   txtHeight.setText("");
                   txtHeight.requestFocus();
                }
       
            }
        });
    }
     public static void main(String[] args) {
        TriFrame frame = new TriFrame();
        frame.setVisible(true);
    }
}
